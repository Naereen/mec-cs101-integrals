You can run the file 'tests.py' to see examples of use of this module 'integrals.py'.
Testing every doctests in the module 'integrals'...
Trying:
    from integrals import *
Expecting nothing
ok
Trying:
    k = 2; xmin = 0; xmax = 3
Expecting nothing
ok
Trying:
    f = lambda x: x**k
Expecting nothing
ok
Trying:
    F = lambda x: x**(k+1) / float(k+1)
Expecting nothing
ok
Trying:
    F(xmax) - F(xmin)
Expecting:
    9.0
ok
Trying:
    riemann_left(f, xmin, xmax, n=8)  # doctest: +ELLIPSIS
Expecting:
    7.382...
ok
Trying:
    riemann_right(f, xmin, xmax, n=8)  # doctest: +ELLIPSIS
Expecting:
    10.757...
ok
Trying:
    riemann_center(f, xmin, xmax, n=8)  # doctest: +ELLIPSIS
Expecting:
    8.964...
ok
Trying:
    trapez(f, xmin, xmax, n=3)
Expecting:
    9.5
ok
Trying:
    exact_int = math.sin(math.pi) - math.sin(0); round(exact_int, 0)
Expecting:
    0.0
ok
Trying:
    boole_int = boole(math.cos, 0, math.pi, n=10)
Expecting nothing
ok
Trying:
    boole_int; abs(round(boole_int, 0))  # doctest: +ELLIPSIS
Expecting:
    1.612...e-16
    0.0
ok
Trying:
    round(100*abs(exact_int - boole_int), 0)  # % Error
Expecting:
    0.0
ok
Trying:
    f = lambda x: (x**3) + (x**2) + (7*x) + 4
Expecting nothing
ok
Trying:
    F = lambda x: (x**4)/4.0 + (x**3)/3.0 + (7 * x**2)/2.0 + (4*x)
Expecting nothing
ok
Trying:
    a, b = -4, 6
Expecting nothing
ok
Trying:
    exact_int2 = F(b) - F(a); round(exact_int2, 0)
Expecting:
    463.0
ok
Trying:
    boole_int2 = boole(f, a, b, n=10)  # n = 10 is good enough!
Expecting nothing
ok
Trying:
    boole_int2; abs(round(boole_int2, 6))  # Weird!
Expecting:
    463.33333333333337
    463.333333
ok
Trying:
    round(100*abs(exact_int2 - boole_int2), 0)  # % Error
Expecting:
    0.0
ok
Trying:
    f = lambda x: (12*x+1)/(1+math.cos(x)**2)
Expecting nothing
ok
Trying:
    a, b = 1993, 2015
Expecting nothing
ok
Trying:
    boole(f, a, b, n=1)  # doctest: +ELLIPSIS
Expecting:
    373463.255...
ok
Trying:
    boole(f, a, b, n=2)  # doctest: +ELLIPSIS
Expecting:
    374343.342...
ok
Trying:
    boole(f, a, b, n=100)  # Really close to the exact value.  # doctest: +ELLIPSIS
Expecting:
    374133.193...
ok
Trying:
    f = lambda x, y:  x**2 + y**2
Expecting nothing
ok
Trying:
    a, b = 0, 1
Expecting nothing
ok
Trying:
    g = lambda x: a
Expecting nothing
ok
Trying:
    h = lambda x: b
Expecting nothing
ok
Trying:
    dbl_quad(f, a, b, g, h, n=1)
Expecting:
    0.5
ok
Trying:
    dbl_quad(f, a, b, g, h, n=2)  # exact from n=2 points
Expecting:
    0.66666666666666674
ok
Trying:
    dbl_quad(f, a, b, g, h, n=40)  # more points do not bring more digits
Expecting:
    0.66666666666666574
ok
Trying:
    f = lambda x, y:  math.cos(x) * y**8
Expecting nothing
ok
Trying:
    a, b = 0, math.pi/2.0
Expecting nothing
ok
Trying:
    g = lambda x: a
Expecting nothing
ok
Trying:
    h = lambda x: b
Expecting nothing
ok
Trying:
    int2d_exact = (b**9) / 9.0; int2d_exact  # doctest: +ELLIPSIS
Expecting:
    6.4689...
ok
Trying:
    dbl_quad(f, a, b, g, h, n=1)  # doctest: +ELLIPSIS
Expecting:
    0.2526...
ok
Trying:
    dbl_quad(f, a, b, g, h, n=2)  # still not very precise for n=2 points  # doctest: +ELLIPSIS
Expecting:
    4.3509...
ok
Trying:
    int2d_approx = dbl_quad(f, a, b, g, h, n=40); int2d_approx  # 13 first digits are perfect  # doctest: +ELLIPSIS
Expecting:
    6.4689...
ok
Trying:
    100 * abs(int2d_exact - int2d_approx) / int2d_exact  # Relative % error, 1e-12 is VERY SMALL  # doctest: +ELLIPSIS
Expecting:
    6.59...e-13
ok
Trying:
    100 * abs(int2d_exact - int2d_approx)  # Absolute % error, 1e-12 is really good!  # doctest: +ELLIPSIS
Expecting:
    4.263...e-12
ok
Trying:
    a1, b1 = 0, 1
Expecting nothing
ok
Trying:
    g1 = lambda x: x; h1 = lambda x: 1
Expecting nothing
ok
Trying:
    f1 = lambda x, y: math.cos(y**2)
Expecting nothing
ok
Trying:
    exact_dbl_int1 = math.sin(1.0) / 2.0; exact_dbl_int1  # doctest: +ELLIPSIS
Expecting:
    0.4207...
ok
Trying:
    dbl_int1 = dbl_quad(f1, a1, b1, g1, h1, n=4)
Expecting nothing
ok
Trying:
    dbl_int1  # doctest: +ELLIPSIS
Expecting:
    0.4207...
ok
Trying:
    100 * abs(dbl_int1 - exact_dbl_int1)  # Not perfect yet, n is too small  # doctest: +ELLIPSIS
Expecting:
    3.933...e-05
ok
Trying:
    dbl_int1 = dbl_quad(f1, a1, b1, g1, h1, n=100)
Expecting nothing
ok
Trying:
    dbl_int1  # doctest: +ELLIPSIS
Expecting:
    0.4207...
ok
Trying:
    100 * abs(dbl_int1 - exact_dbl_int1)  # Almost perfect computation (13 digits are correct)  # doctest: +ELLIPSIS
Expecting:
    0.0
ok
Trying:
    a2, b2 = 0, 1
Expecting nothing
ok
Trying:
    g2 = lambda x: x**2; h2 = lambda x: x**0.5
Expecting nothing
ok
Trying:
    f2 = lambda x, y: 1
Expecting nothing
ok
Trying:
    exact_dbl_int2 = 1.0 / 3.0
Expecting nothing
ok
Trying:
    dbl_int2 = dbl_quad(f2, a2, b2, g2, h2, n=100)
Expecting nothing
ok
Trying:
    dbl_int2  # doctest: +ELLIPSIS
Expecting:
    0.3333...
ok
Trying:
    100 * abs(dbl_int2 - exact_dbl_int2)  # 0.0001% is very good!  # doctest: +ELLIPSIS
Expecting:
    1.01432...e-05
ok
Trying:
    a3, b3 = 0, 1
Expecting nothing
ok
Trying:
    g3 = lambda x: x; h3 = lambda x: 1
Expecting nothing
ok
Trying:
    f3 = lambda x, y: math.sin(y) / y
Expecting nothing
ok
Trying:
    exact_dbl_int3 = 1 - math.cos(1.0); exact_dbl_int3
Expecting:
    0.45969769413186023
ok
Trying:
    dbl_int3 = dbl_quad(f3, a3, b3, g3, h3, n=100)
Expecting nothing
ok
Trying:
    dbl_int3
Expecting:
    0.4596976941318604
ok
Trying:
    100 * abs(dbl_int3 - exact_dbl_int3)  # Almost perfect computation (14 digits are correct)  # doctest: +ELLIPSIS
Expecting:
    1.665...e-14
ok
Trying:
    f = lambda x: (12*x+1)/(1+math.cos(x)**2)
Expecting nothing
ok
Trying:
    a, b = 1993, 2015
Expecting nothing
ok
Trying:
    gaussian_quad(f, a, b, n=1)  # doctest: +ELLIPSIS
Expecting:
    279755.057...
ok
Trying:
    gaussian_quad(f, a, b, n=3)  # doctest: +ELLIPSIS
Expecting:
    343420.473...
ok
Trying:
    gaussian_quad(f, a, b, n=100)  # Quite accurate result, see above.  # doctest: +ELLIPSIS
Expecting:
    374133.206...
ok
Trying:
    random.seed(1)  # same values all the time
Expecting nothing
ok
Trying:
    xmin, xmax = 1, 6
Expecting nothing
ok
Trying:
    f = lambda x: x  # simple example
Expecting nothing
ok
Trying:
    intf = (xmax**2 / 2.0) - (xmin**2 / 2.0); intf
Expecting:
    17.5
ok
Trying:
    ymin, ymax = xmin, xmax
Expecting nothing
ok
Trying:
    n = 100
Expecting nothing
ok
Trying:
    intf_apporx = montecarlo(f, xmin, xmax, n, ymin, ymax); intf_apporx
Expecting:
    18.0
ok
Trying:
    100 * abs(intf - intf_apporx) / abs(intf)  # Relative % error, 2.8%  # doctest: +ELLIPSIS
Expecting:
    2.857...
ok
Trying:
    n = 100000
Expecting nothing
ok
Trying:
    intf_apporx = montecarlo(f, xmin, xmax, n, ymin, ymax); intf_apporx  # doctest: +ELLIPSIS
Expecting:
    17.444...
ok
Trying:
    100 * abs(intf - intf_apporx) / abs(intf)  # Relative % error, 0.32%  # doctest: +ELLIPSIS
Expecting:
    0.318...
ok
Trying:
    n = 100000
Expecting nothing
ok
Trying:
    intf_apporx = montecarlo(f, xmin, xmax, n); intf_apporx  # doctest: +ELLIPSIS
Expecting:
    17.485...
ok
Trying:
    100 * abs(intf - intf_apporx) / abs(intf)  # Relative % error, 0.08% is really good!  # doctest: +ELLIPSIS
Expecting:
    0.0844...
ok
Trying:
    R = 1
Expecting nothing
ok
Trying:
    f = lambda X: 1
Expecting nothing
ok
Trying:
    V_3 = (4.0/3.0) * math.pi * (R**3); V_3  # doctest: +ELLIPSIS
Expecting:
    4.18879...
ok
Trying:
    isInside = lambda X: 1 if (sum(x**2 for x in X) <= R**2) else 0
Expecting nothing
ok
Trying:
    F = lambda X: f(X) * isInside(X)
Expecting nothing
ok
Trying:
    Xmin = [0, 0, 0]; Xmax = [R, R, R]
Expecting nothing
ok
Trying:
    random.seed(1)  # same values all the time
Expecting nothing
ok
Trying:
    (2**3) * nd_montecarlo(F, Xmin, Xmax, n=10)  # doctest: +ELLIPSIS
Expecting:
    3.2159...
ok
Trying:
    (2**3) * nd_montecarlo(F, Xmin, Xmax, n=100)  # doctest: +ELLIPSIS
Expecting:
    3.9395...
ok
Trying:
    V_approx1000 = (2**3) * nd_montecarlo(F, Xmin, Xmax, n=1000); V_approx1000  # doctest: +ELLIPSIS
Expecting:
    4.19687...
ok
Trying:
    100 * abs(V_3 - V_approx1000) / abs(V_3)  # Relative % error, 0.19% is already very good!  # doctest: +ELLIPSIS
Expecting:
    0.193...
ok
Trying:
    V_approx10000 = (2**3) * nd_montecarlo(F, Xmin, Xmax, n=10000); V_approx10000  # doctest: +ELLIPSIS
Expecting:
    4.25637...
ok
Trying:
    100 * abs(V_3 - V_approx10000) / abs(V_3)  # Relative % error, 1.6% is less accurate. Why?  # doctest: +ELLIPSIS
Expecting:
    1.613...
ok
Trying:
    R = 1
Expecting nothing
ok
Trying:
    f = lambda X: 1
Expecting nothing
ok
Trying:
    V_3 = (4.0/3.0) * math.pi * (R**3); V_3  # doctest: +ELLIPSIS
Expecting:
    4.18879...
ok
Trying:
    isInside = lambda X: 1 if (sum(x**2 for x in X) <= R**2) else 0
Expecting nothing
ok
Trying:
    F = lambda X: f(X) * isInside(X)
Expecting nothing
ok
Trying:
    Xmin = [0, 0, 0]; Xmax = [R, R, R]
Expecting nothing
ok
Trying:
    (2**3) * nd_quad(F, Xmin, Xmax, n=2)
Expecting:
    4.0
ok
Trying:
    (2**3) * nd_quad(F, Xmin, Xmax, n=4)
Expecting:
    4.0
ok
Trying:
    (2**3) * nd_quad(F, Xmin, Xmax, n=8)
Expecting:
    4.3182389695603307
ok
Trying:
    V_approx10 = (2**3) * nd_quad(F, Xmin, Xmax, n=10); V_approx10  # doctest: +ELLIPSIS
Expecting:
    4.12358...
ok
Trying:
    100 * abs(V_3 - V_approx10) / abs(V_3)  # Relative % error, 1.5% is OK  # doctest: +ELLIPSIS
Expecting:
    1.55...
ok
Trying:
    V_approx40 = (2**3) * nd_quad(F, Xmin, Xmax, n=40); V_approx40  # doctest: +ELLIPSIS
Expecting:
    4.18170...
ok
Trying:
    100 * abs(V_3 - V_approx40) / abs(V_3)  # Relative % error, 0.16% is good  # doctest: +ELLIPSIS
Expecting:
    0.16...
ok
Trying:
    from math import gamma, pi
Expecting nothing
ok
Trying:
    V = lambda k, R: (pi**(k/2.0))/(gamma( 1 + k/2.0 )) * (R**k)
Expecting nothing
ok
Trying:
    k = 5; R = 1
Expecting nothing
ok
Trying:
    V_5 = V(k, R); V_5  # Exact value!  # doctest: +ELLIPSIS
Expecting:
    5.26378...
ok
Trying:
    Xmin = [0]*k; Xmax = [1]*k
Expecting nothing
ok
Trying:
    isInside = lambda X: 1 if (sum(x**2 for x in X) <= R**2) else 0
Expecting nothing
ok
Trying:
    F = lambda X: 1.0 * isInside(X)
Expecting nothing
ok
Trying:
    V_approx5_3 = (2**k) * nd_quad(F, Xmin, Xmax, n=3)  # 3**5 = 243 points, so really not accurate
Expecting nothing
ok
Trying:
    V_approx5_3  # doctest: +ELLIPSIS
Expecting:
    4.2634...
ok
Trying:
    100 * abs(V_5 - V_approx5_3) / abs(V_5)  # n=3 gives an error of 19%, that's not too bad!  # doctest: +ELLIPSIS
Expecting:
    19.0049...
ok
Trying:
    V_approx5_10 = (2**k) * nd_quad(F, Xmin, Xmax, n=10)  # 10**5 = 10000 points!
Expecting nothing
ok
Trying:
    V_approx5_10  # doctest: +ELLIPSIS
Expecting:
    5.25263...
ok
Trying:
    100 * abs(V_5 - V_approx5_10) / abs(V_5)  # Pretty good!  # doctest: +ELLIPSIS
Expecting:
    0.211...
ok
Trying:
    V_approx5_15 = (2**k) * nd_quad(F, Xmin, Xmax, n=15)  # 15**5 = 759375 points!
Expecting nothing
ok
Trying:
    V_approx5_15  # doctest: +ELLIPSIS
Expecting:
    5.24665...
ok
Trying:
    100 * abs(V_5 - V_approx5_15) / abs(V_5)  # 0.32%, that's great!  # doctest: +ELLIPSIS
Expecting:
    0.325...
ok
Trying:
    V_approx5_16 = (2**k) * nd_quad(F, Xmin, Xmax, n=16)  # 16**5 = 1048576 points!
Expecting nothing
ok
Trying:
    V_approx5_16  # doctest: +ELLIPSIS
Expecting:
    5.263061...
ok
Trying:
    100 * abs(V_5 - V_approx5_16) / abs(V_5)  # 0.01%, that's great!  # doctest: +ELLIPSIS
Expecting:
    0.013...
ok
Trying:
    random.seed(1)  # same values all the time
Expecting nothing
ok
Trying:
    ymin_exact, ymax_exact = 0, 1
Expecting nothing
ok
Trying:
    Xmin = [0, 0]; Xmax = [1, 1]
Expecting nothing
ok
Trying:
    F = lambda X: 1 if (sum(x**2 for x in X) <= 1) else 0
Expecting nothing
ok
Trying:
    ymin, ymax = nd_yminmax(F, Xmin, Xmax, 100)
Expecting nothing
ok
Trying:
    ymin, ymax
Expecting:
    (0.0, 1.005)
ok
Trying:
    100 * abs(ymin - ymin_exact)  # Absolute % error < 0.5%
Expecting:
    0.0
ok
Trying:
    100 * abs(ymax - ymax_exact)  # Absolute % error < 0.5%
Expecting:
    0.49999999999998934
ok
Trying:
    random.seed(1)  # same values all the time
Expecting nothing
ok
Trying:
    random_point([0, 0, 0, 0], [1, 2, 3, 4], 4)  # doctest: +ELLIPSIS
Expecting:
    [0.134..., 1.694..., 2.291..., 1.020...]
ok
Trying:
    exact_int = math.sin(math.pi) - math.sin(0); round(exact_int, 0)
Expecting:
    0.0
ok
Trying:
    center_int = riemann_center(math.cos, 0, math.pi, n=15); center_int  # doctest: +ELLIPSIS
Expecting:
    2.918...e-16
ok
Trying:
    round(100*abs(exact_int - center_int), 0)  # % Error
Expecting:
    0.0
ok
Trying:
    exact_int = math.sin(math.pi) - math.sin(0)
Expecting nothing
ok
Trying:
    exact_int  # doctest: +ELLIPSIS
Expecting:
    1.22...e-16
ok
Trying:
    round(exact_int, 0)
Expecting:
    0.0
ok
Trying:
    left_int = riemann_left(math.cos, 0, math.pi, n=15); left_int  # doctest: +ELLIPSIS
Expecting:
    0.2094...
ok
Trying:
    round(100*abs(exact_int - left_int), 0)  # Relative % error of 21%, VERY BAD!
Expecting:
    21.0
ok
Trying:
    exact_int = math.sin(math.pi) - math.sin(0); round(exact_int, 0)
Expecting:
    0.0
ok
Trying:
    right_int = riemann_right(math.cos, 0, math.pi, n=15); right_int  # doctest: +ELLIPSIS
Expecting:
    -0.2094...
ok
Trying:
    round(100*abs(exact_int - right_int), 0)  # % Error
Expecting:
    21.0
ok
Trying:
    right_int = riemann_right(math.cos, 0, math.pi, n=2000); right_int  # doctest: +ELLIPSIS
Expecting:
    -0.00157...
ok
Trying:
    100*abs(exact_int - right_int)  # Error is less than 0.15 %  # doctest: +ELLIPSIS
Expecting:
    0.15...
ok
Trying:
    round(100*abs(exact_int - right_int), 0)  # % Error
Expecting:
    0.0
ok
Trying:
    f = lambda x: (2.0 / math.sqrt(math.pi)) * math.exp(-x**2)
Expecting nothing
ok
Trying:
    erf1 = romberg(f, 0, 1, 5, 5); erf1  # doctest: +ELLIPSIS
Expecting:
    0.84270...
ok
Trying:
    exact_erf1 = 0.842700792949715  # From Wikipedia
Expecting nothing
ok
Trying:
    100 * abs(erf1 - exact_erf1)  # Absolute % error, 2e-11 is almost perfect!  # doctest: +ELLIPSIS
Expecting:
    2.070...e-11
ok
Trying:
    area = romberg(math.sin, 0, math.pi, 5, 5); area
Expecting:
    2.0000000000013207
ok
Trying:
    100 * abs(area - 2.0)  # Absolute % error, 1e-10 is already very good!  # doctest: +ELLIPSIS
Expecting:
    1.320...e-10
ok
Trying:
    area2 = romberg(math.sin, 0, 1001*math.pi, 5, 5); area2  # doctest: +ELLIPSIS
Expecting:
    -148.929...
ok
Trying:
    100 * abs(area2 - 2.0)  # Really bad here!  # doctest: +ELLIPSIS
Expecting:
    15092.968...
ok
Trying:
    area3 = romberg(math.sin, 0, 1001*math.pi, 15, 15); area3  # doctest: +ELLIPSIS
Expecting:
    1.999...
ok
Trying:
    100 * abs(area3 - 2.0)  # Should be better: yes indeed, an absolute error of 3e-09 % is quite good!  # doctest: +ELLIPSIS
Expecting:
    3.145...e-09
ok
Trying:
    n = m = 5
Expecting nothing
ok
Trying:
    a = 0; b = 1
Expecting nothing
ok
Trying:
    f1 = lambda x: (1-x**2)**0.5
Expecting nothing
ok
Trying:
    int_f1 = romberg(f1, a, b, n, m); int_f1  # doctest: +ELLIPSIS
Expecting:
    0.784...
ok
Trying:
    100 * abs(int_f1 - math.pi / 4.0)  # 0.05%, that's really good!  # doctest: +ELLIPSIS
Expecting:
    0.053...
ok
Trying:
    f2 = lambda x: x**2
Expecting nothing
ok
Trying:
    int_f2 = romberg(f2, a, b, n, m); int_f2
Expecting:
    0.3333333333333333
ok
Trying:
    100 * abs(int_f2 - 1.0/3.0)
Expecting:
    0.0
ok
Trying:
    f3 = math.sin; b = math.pi
Expecting nothing
ok
Trying:
    int_f3 = romberg(f3, a, b, n, m); int_f3
Expecting:
    2.0000000000013207
ok
Trying:
    100 * abs(int_f3 - 2.0)  # 10e-10 % error, that's almost perfect!  # doctest: +ELLIPSIS
Expecting:
    1.320...e-10
ok
Trying:
    f4 = math.exp
Expecting nothing
ok
Trying:
    n, m = 5, 5
Expecting nothing
ok
Trying:
    a, b = -4, 19
Expecting nothing
ok
Trying:
    int_f4 = romberg(f4, a, b, n, m); int_f4  # doctest: +ELLIPSIS
Expecting:
    178495315.533...
ok
Trying:
    exact_f4 =  f4(b) - f4(a); exact_f4  # doctest: +ELLIPSIS
Expecting:
    178482300.944...
ok
Trying:
    100 * abs(int_f4 - exact_f4)  # Not so good result! n=m=5 is not enough  # doctest: +ELLIPSIS
Expecting:
    1301458.822...
ok
Trying:
    n, m = 10, 10   # More points!
Expecting nothing
ok
Trying:
    int_f4_2 = romberg(f4, a, b, n, m); int_f4_2  # doctest: +ELLIPSIS
Expecting:
    178482300.944...
ok
Trying:
    exact_f4_2 =  f4(b) - f4(a)
Expecting nothing
ok
Trying:
    100 * abs(int_f4_2 - exact_f4_2)  # A smaller error!  # doctest: +ELLIPSIS
Expecting:
    5.960...e-06
ok
Trying:
    a, b = -1000, 20; n, m = 10, 10   # More points!
Expecting nothing
ok
Trying:
    int_f4_3 = romberg(f4, a, b, n, m); int_f4_3  # doctest: +ELLIPSIS
Expecting:
    485483299.278...
ok
Trying:
    exact_f4_3 =  f4(b) - f4(a); exact_f4_3  # doctest: +ELLIPSIS
Expecting:
    485165195.409...
ok
Trying:
    100 * abs(int_f4_3 - exact_f4_3)  # It is not accurate for big intervals  # doctest: +ELLIPSIS
Expecting:
    31810386.832...
ok
Trying:
    a, b = -1000, 20; n, m = 20, 20   # More points! n=m=20 is really big!
Expecting nothing
ok
Trying:
    int_f4_4 = romberg(f4, a, b, n, m); int_f4_4  # doctest: +ELLIPSIS
Expecting:
    485165195.409...
ok
Trying:
    100 * abs(int_f4_4 - exact_f4_3)
Expecting:
    0.0
ok
Trying:
    f = lambda x: (12*x+1)/(1+math.cos(x)**2)
Expecting nothing
ok
Trying:
    a, b = 1993, 2015
Expecting nothing
ok
Trying:
    romberg(f, a, b, n=0)  # really not accurate!  # doctest: +ELLIPSIS
Expecting:
    477173.613...
ok
Trying:
    romberg(f, a, b, n=1)  # doctest: +ELLIPSIS
Expecting:
    345561.243...
ok
Trying:
    romberg(f, a, b, n=2)  # doctest: +ELLIPSIS
Expecting:
    373463.255...
ok
Trying:
    romberg(f, a, b, n=3)  # doctest: +ELLIPSIS
Expecting:
    374357.311...
ok
Trying:
    romberg(f, a, b, n=5)  # doctest: +ELLIPSIS
Expecting:
    374134.549...
ok
Trying:
    romberg(f, a, b, n=8)  # Almost the exact value.  # doctest: +ELLIPSIS
Expecting:
    374133.192...
ok
Trying:
    romberg(f, a, b, n=10)  # More precise!  # doctest: +ELLIPSIS
Expecting:
    374133.193...
ok
Trying:
    romberg(f, a, b, n=14)  # Again more precise!  # doctest: +ELLIPSIS
Expecting:
    374133.193...
ok
Trying:
    romberg(f, a, b, n=20)  # Exact value!  # doctest: +ELLIPSIS
Expecting:
    374133.193...
ok
Trying:
    f = lambda x: (12*x+1)/(1+math.cos(x)**2)
Expecting nothing
ok
Trying:
    a, b = 1993, 2015
Expecting nothing
ok
Trying:
    romberg_rec(f, a, b, n=0)  # really not accurate!  # doctest: +ELLIPSIS
Expecting:
    477173.613...
ok
Trying:
    romberg_rec(f, a, b, n=1)  # alreay pretty good!  # doctest: +ELLIPSIS
Expecting:
    345561.243...
ok
Trying:
    romberg_rec(f, a, b, n=2)  # doctest: +ELLIPSIS
Expecting:
    373463.255...
ok
Trying:
    romberg_rec(f, a, b, n=3)  # doctest: +ELLIPSIS
Expecting:
    374357.311...
ok
Trying:
    romberg_rec(f, a, b, n=8)  # Almost the exact value.  # doctest: +ELLIPSIS
Expecting:
    374133.192...
ok
Trying:
    exact_int = math.sin(math.pi) - math.sin(0); round(exact_int, 0)
Expecting:
    0.0
ok
Trying:
    simpson_int = simpson(math.cos, 0, math.pi, n=10)
Expecting nothing
ok
Trying:
    simpson_int; abs(round(simpson_int, 0))  # doctest: +ELLIPSIS
Expecting:
    9.300...e-17
    0.0
ok
Trying:
    round(100*abs(exact_int - simpson_int), 0)  # % Error
Expecting:
    0.0
ok
Trying:
    f = lambda x: (x**2) + (7*x) + (4)
Expecting nothing
ok
Trying:
    F = lambda x: ((x**3)/3.0) + ((7 * x**2)/2.0) + (4*x)
Expecting nothing
ok
Trying:
    a, b = -1, 12
Expecting nothing
ok
Trying:
    exact_int2 = F(b) - F(a); round(exact_int2, 0)
Expecting:
    1129.0
ok
Trying:
    simpson_int2 = simpson(f, a, b, n=2)
Expecting nothing
ok
Trying:
    simpson_int2; abs(round(simpson_int2, 0))
Expecting:
    1128.8333333333333
    1129.0
ok
Trying:
    round(100*abs(exact_int2 - simpson_int2), 0)  # % Error
Expecting:
    0.0
ok
Trying:
    round(simpson(lambda x:x**3, 0.0, 10.0, 2), 7)
Expecting:
    2500.0
ok
Trying:
    round(simpson(lambda x:x**3, 0.0, 10.0, 10000), 7)
Expecting:
    2500.0
ok
Trying:
    round(simpson(lambda x:x**4, 0.0, 10.0, 2), 7)
Expecting:
    20833.3333333
ok
Trying:
    round(simpson(lambda x:x**4, 0.0, 10.0, 100000), 7)
Expecting:
    20000.0
ok
Trying:
    f = lambda x: (12*x+1) / (1+math.cos(x)**2)
Expecting nothing
ok
Trying:
    a, b = 1993, 2015
Expecting nothing
ok
Trying:
    simpson(f, a, b, n=2)  # doctest: +ELLIPSIS
Expecting:
    345561.243...
ok
Trying:
    simpson(f, a, b, n=8)  # doctest: +ELLIPSIS
Expecting:
    374179.344...
ok
Trying:
    simpson(f, a, b, n=100)  # doctest: +ELLIPSIS
Expecting:
    374133.138...
ok
Trying:
    exact_int = math.sin(math.pi) - math.sin(0); round(exact_int, 0)
Expecting:
    0.0
ok
Trying:
    trapez_int = trapez(math.cos, 0, math.pi, n=15); trapez_int  # doctest: +ELLIPSIS
Expecting:
    2.281...e-16
ok
Trying:
    round(100*abs(exact_int - trapez_int), 0)  # % Error
Expecting:
    0.0
ok
Trying:
    random.seed(1)  # same values all the time
Expecting nothing
ok
Trying:
    ymin_exact, ymax_exact = -0.25, 12
Expecting nothing
ok
Trying:
    ymin, ymax = yminmax(lambda x: x**2 + x, -2, 3, 200)
Expecting nothing
ok
Trying:
    ymin, ymax  # doctest: +ELLIPSIS
Expecting:
    (-0.251..., 12.059...)
ok
Trying:
    100 * abs(ymin - ymin_exact) / abs(ymin_exact)  # Relative % error < 0.5%  # doctest: +ELLIPSIS
Expecting:
    0.480...
ok
Trying:
    100 * abs(ymax - ymax_exact) / abs(ymax_exact)  # Relative % error < 0.5%  # doctest: +ELLIPSIS
Expecting:
    0.499...
ok
6 items had no tests:
    __main__.plot_montecarlo
    __main__.plot_riemann_center
    __main__.plot_riemann_left
    __main__.plot_riemann_right
    __main__.plot_trapez
    __main__.xw_gauss_legendre
17 items passed all tests:
   9 tests in __main__
  16 tests in __main__.boole
  41 tests in __main__.dbl_quad
   5 tests in __main__.gaussian_quad
  14 tests in __main__.montecarlo
  13 tests in __main__.nd_montecarlo
  32 tests in __main__.nd_quad
   8 tests in __main__.nd_yminmax
   2 tests in __main__.random_point
   3 tests in __main__.riemann_center
   5 tests in __main__.riemann_left
   6 tests in __main__.riemann_right
  49 tests in __main__.romberg
   7 tests in __main__.romberg_rec
  20 tests in __main__.simpson
   3 tests in __main__.trapez
   6 tests in __main__.yminmax
239 tests in 23 items.
239 passed and 0 failed.
Test passed.

More details about doctest can be found on the Python documentation: 
https://docs.python.org/2/library/doctest.html
