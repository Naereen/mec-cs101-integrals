Readme
======
This Python (v2 or v3) project gives an (almost) complete solution for the CS101 programming project, subject #6, about  **Numerical Integration techniques**
This project took place at `Mahindra Ecole Centrale <http://www.mahindraecolecentrale.edu.in/>`_ in April 2015.

Inside this directory, you will find two Python files (``integrals.py`` and ``tests.py``).

`integrals <https://mec-cs101-integrals.readthedocs.io/en/latest/integrals.html>`_
-----------------------------------------------------------------------------------
Defines the integration techniques and algorithms.

`tests <https://mec-cs101-integrals.readthedocs.io/en/latest/tests.html>`_
----------------------------------------------------------------------------
Performs many tests and examples, by using the `integrals <https://mec-cs101-integrals.readthedocs.io/en/latest/integrals.html>`_ module.

-----------------------------------------------------------------------------

Documentation
-------------
.. image:: https://readthedocs.org/projects/mec-cs101-integrals/badge/?version=latest
   :target: https://readthedocs.org/projects/mec-cs101-integrals/?badge=latest
   :alt: Documentation Status


The documentation is hosted on `ReadTheDocs <https://readthedocs.org/>`_:
`https://mec-cs101-integrals.readthedocs.io/ <https://mec-cs101-integrals.readthedocs.io/>`_.

-----------------------------------------------------------------------------

Other files
-----------
Please read:

 - ``INSTALL.txt`` : for details about using or installing these files.
 - the report, ``Numerical_Integration__Project_CS101_2015.pdf``, gives more details about the Python programs, and theoretical explanations about the algorithms we decided to implement, and more small things.
 - ``AUTHORS.txt`` : gives a complete list of authors.
 - ``TODO.txt`` : gives details about un-finished tasks, if you want to conclude the project yourself.
 - ``LICENSE.txt`` : for details about the license under which this project is publicly released.

-----------------------------------------------------------------------------

About this file
---------------
It quickly explains what your project was about.
It should sum up in a few lines what was the task, and how you solved it.

Imagine that someone downloaded your project and want to understand it, well then this file should be as helpful as possible (while not being too long or verbous).
It should be the starting point for a new user.
